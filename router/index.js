import express from 'express';
import json from 'body-parser';
export const router = express.Router();


router.get('/', (req,res)=>{
 
    const params = {
        numeroRecibo: req.query.numeroRecibo,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        tipoServicio: req.query.tipoServicio,
        kilowattsConsumidos: req.query.kilowattsConsumidos,
     
    }
    res.render('index',params);
})

router.post('/', (req,res)=>{

    const params = {
        numeroRecibo: req.body.numeroRecibo,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        tipoServicio: req.body.tipoServicio,
        kilowattsConsumidos: req.body.kilowattsConsumidos,

    }
    res.render('index',params);
})

export default {router}